<div class="reportAbuseWrapper">
	<?php
	$this->widget('ReportAbuse', array(
			'params'=>array(
				'objectId'=>$qaViewModel->question->id,
				'objectType'=>'question',
				'tipsyClass'=>'tipsyleft',
				'authorId'=>$qaViewModel->question->author->id,
				'reportStatus'=>$qaViewModel->question->reportAbuse
			)
	));
	?>
</div>