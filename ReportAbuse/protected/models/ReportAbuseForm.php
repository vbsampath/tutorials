<?php

/**
 * This is base Report Abuse Form model. It can be used for multiple objects.
 * Feel free to extend this form for specific bussiness requirements.
 * Be careful while implementing same form for multiple scenarios and extending it.
 * Use {@link CModel::scenario} to solve such issues.
 *  
 * @author Sampath Vangari <sampath.vangari@WT.com>
 *
 */
class ReportAbuseForm extends CFormModel {

	/**
	 * Message given by users while reporting an object.
	 * @var string
	 */
    public $comment;
    
    /**
     * unique identifier for object to be reported.
     * @var int
     */
    public $objectId;
    
    /**
     * unique identifier for object parent to be reported.
     * @var int
     */
    public $parentId;
    
    /**
     * unique identifier to say the object type ie., group, discussion, question, answer
     * @var string
     */
    public $objectType;
    
    /**
     * array giving more information on the operation like module qa, advisoryboard, group (qa and ab report objects may be similar but they are different)
     * @var array
     */
    public $context;
    
    
    public function rules() {
        return array(
        	array('comment', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify'),'message'=>Yii::t('yii','xss.attack')),
            array('comment', 'required', 'message' => Yii::t('yii', 'reportAbuse.comment')),
            array('comment', 'length', 'max'=>250,'tooLong' => Yii::t('yii', 'reportAbuse.comment.length')),
        	array('comment,objectId,objectType,parentId,context','safe'),
        );
    }

    public function arrtibuteLabels() {
        return array(
            'comment' => 'Comment',
        );
    }
}

?>
