<?php
/**
 * Description of SiteModel
 *
 * This model contains all utility data methods which are used throughout site.
 *
 * Lets say we have a functionality which is repeating in each and every module then we take all that logic to here and consolidate it.
 *
 * @author Sampath Vangari <vbsampath@gmail.com>
 */
class SiteModel {
	
	/**
	 * This method serves all report abuse java calls.
	 * <b>$reportAbuseForm</b> will have necessary properties set for various contexts.
	 * <b>$context</b> will be an array which will give more information about the current operation.
	 * This is not yet implemented for now but if at all its implemented then one can pass context related information to do more operations.
	 * @param ReportAbuseForm $reportAbuseForm report abuse form set by ui from different contexts like questions,answers,groups,discussions etc.,
	 * @param array $context more information on the current operation like module, controller etc.,
	 * @throws Exception $ex
	 * 
	 * @author Sampath Vangari <sampath.vangari@WT.com>
	 */
	public function reportAbuse($reportAbuseForm, $context)
	{
		try {
			switch ($reportAbuseForm->objectType) {
				//write your parameter binding stuff and call database.
				case 'question':
					$reposObj = new Java("com.ie.qa.QARepository");
					$classObj = new Java("com.ie.qa.QAs");
					$method = 'insertReportAbuseQuestion';
					$params = array(
							array('p_QuestionId', $reportAbuseForm->objectId, 'Int'),
							array('p_Comment', $reportAbuseForm->comment, 'String'),
							array('p_UserId', Yii::app()->user->id, 'Int'),
					);
					break;
				case 'answer':
					$reposObj = new Java("com.ie.qa.QARepository");
					$classObj = new Java("com.ie.qa.QAs");
					$method = 'insertReportAbuseAnswer';
					$params = array(
							array('p_QuestionId', $reportAbuseForm->parentId, 'Int'),
							array('p_AnswerId', $reportAbuseForm->objectId, 'Int'),
							array('p_Comment', $reportAbuseForm->comment, 'String'),
							array('p_UserId', Yii::app()->user->id, 'Int'),
					);
					break;
				case 'group':
					$reposObj = new Java("com.ie.groups.GroupRepository");
					$classObj = new Java("com.ie.groups.GroupDetails");
					$method = 'reportAbuseGroupInsert';
					$params = array(
							array('p_GroupId', $reportAbuseForm->objectId, 'Int'),
							array('p_Comment', $reportAbuseForm->comment, 'String'),
							array('p_UserId', Yii::app()->user->id, 'Int'),
					);
					break;
				case 'discussion':
					$reposObj = new Java("com.ie.groups.GroupRepository");
					$classObj = new Java("com.ie.groups.GroupDetails");
					$method = 'insertReportAbuseGroupDiscussion';
					$params = array(
							array('p_GroupId', $reportAbuseForm->parentId, 'Int'),
							array('p_GroupDiscussionId', $reportAbuseForm->objectId, 'Int'),
							array('p_Comment', $reportAbuseForm->comment, 'String'),
							array('p_UserId', Yii::app()->user->id, 'Int'),
					);
					break;
			}
			//Utils::printFormattedArray($params);exit;
			//call your database to get desired result
			$isCreated = $classObj->$method($reposObj, IEJavaModel::bindParams($params));

		} catch (Exception $ex) {
			throw $ex;
		}
		return $isCreated;
	}
}
?>