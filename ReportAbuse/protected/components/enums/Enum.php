<?php
/**
 * Superclass for any Enum.
 * 
 * Usage : 
 *
 * <pre>
 * class JavaDataTypes extends Enum
 * {
 * &nbsp;&nbsp;&nbsp;&nbsp;const STRING = "String";
 * &nbsp;&nbsp;&nbsp;&nbsp;const Int = "Int";
 * }
 * </pre>
 * 
 * One can feel free to implement some base methods in this abstract class which all derived classes will use.
 * for more information {@link http://captainkuro.com/php/enums-in-php/}
 *
 *
 * @author Sampath Vangari <sampath.vangari@WT.com>
 */
abstract class Enum
{

    /**
     * Prevent creating from outside. Caches all enum values defined in
     * subclass
     */
    private function __construct()
    {
		
    }

    /**
     * Prevent cloning from outside
     */
    private function __clone()
    {

    }
}
?>