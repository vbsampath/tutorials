<?php

/**
 * WidgetSizes defines sizes for widgets for different scenarios.
 * 
 * Use these standard sizes for all further development of UI blocks.
 * This way we can be sure that our UI will be on certain standards and changes will also be easy in future.
 * 
 * Please feel free to extend this class for further bussiness requirements.
 * 
 * 
 * @author Sampath Vangari <vbsampath@gmail.com>
 *
 */ 
class WidgetSizes extends Enum
{
	/**
	 * Similar to profile send message. Ranges from 150-200px square
	 * @var string
	 */
	const Tiny = "Tiny";
	
	/**
	 * Similar to report abuse in profile. Ranges from 200-300px square
	 * @var string
	 */
    const Small = "Small";
    
    /**
     * Similar to child comments. Ranges from 500-600px rectangle
     * @var string
     */
    const Medium = "Medium";
    
    /**
     * Similar to parent comments. Ranges from 600-800px rectangle
     * @var string
     */
    const MediumLarge = "MediumLarge";
    
    /**
     * Any other case where above sizes wont fit in. Not yet decided.
     * @var string
     */
    const Large = "Large"; 
    
    public static function enum($string){
    	return constant('WidgetSizes::'.$string);
    }
}
?>