<?php
/**
 * Report Abuse Widget.
 * 
 * @package application\components
 * @author Sampath Vangari <sampath.vangari@WT.com>
 */ 
//namespace App\Enums;
class ReportAbuse extends CWidget{
    
	public $params;
	
    public function init() {
    	
    }
    
    public function run() {
        
    	$error = new ICrowdError;
    	$reportAbuseForm = new ReportAbuseForm;
    	$parentId = '';
    	$context = '';
    	try {
    		$params = $this->params;
    		//$widgetSize = App\Enums\WidgetSizes::enum("Small");
    		//Utils::printFormattedArray($widgetSize);exit;
    		//$htmlOptions = $this->htmlOptions;
    	
    		// handle exception when $params is not set
    		if (!isset($params)) {
    			throw new Exception(Yii::t('yii', 'incorrect.params.message'));
    		}
    	
    		// handle exception when $params is not set
    		if (is_null($params)) {
    			throw new Exception(Yii::t('yii', 'incorrect.params.message'));
    		}
    		
    		if(isset($params['parentId']))
    			$parentId = $params['parentId'];
    		if(isset($params['context']))
    			$context = $params['context'];
    		
    		$reportAbuseForm->objectId = $params['objectId'];
    		$reportAbuseForm->objectType = $params['objectType'];
    		$reportAbuseForm->parentId = $parentId;
    		$reportAbuseForm->context = $context;
    		
    		if($context == 'profile')
    		{
    			$params['widgetSize'] = WidgetSizes::Small;
    		}
    		
    		//Utils::printFormattedArray($params);exit;
    	
    	} catch (Exception $ex) {
    		//Utils::printFormattedArray($ex);
    		$error->message = $ex->getMessage();
    	}
    	$this->render('reportabuse/reportabuse', 
    			array(
    				'reportAbuseForm'=>$reportAbuseForm,
    				'params'=>$params,
	    			'error' => $error
    			)
    	);
    }    
}
?>