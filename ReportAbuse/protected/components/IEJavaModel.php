<?php
/**
 * IEJavaModel represents a  model that has connectivity Zend JAVA Bridge for data access
 *
 * You may override
 * {@link rules()} to declare validation rules that should be applied to
 * the attributes.
 * 
 * @package application\components
 * @author Bharat Kumar Jonnalagadda <bharat.jonnalagadda@ideaentity.com>
 * @version 1.00
 */
class IEJavaModel extends CModel
{
	
	/**
	 * Constructor.
	 * Creating application related one time objects
	 * @param string $scenario name of the scenario that this model is used in.
	 * See {@link CModel::scenario} on how scenario is used by models.
	 * @see getScenario
	 */
	public function __construct($scenario='')
	{
		$this->setScenario($scenario);
		$this->init();
		$this->attachBehaviors($this->behaviors());
		$this->afterConstruct();
	}

	
	/**
	 * Initializes this model.
	 * This method is invoked in the constructor right after {@link scenario} is set.
	 * You may override this method to provide code that is needed to initialize the model (e.g. setting
	 * initial property values.)
	 */
	public function init()
	{
	}

	
	/**
	 * Binds params using params array into java params list.
	 * 
	 * It eases development by using some standard set of rules.
	 * It binds all required parameters using one call.
	 * 
	 * @param array $params PHP array consisting of all parameters required for stored procedure.
	 * @return mixed $lstParam list of params.
	 * 
	 * @author Sampath Vangari <sampath.vangari@WT.com>
	 */
	public static function bindParams($params)
	{
		try {
			
			$arrayLength = count($params);
			$lstParam = new Java("java.util.ArrayList");
			
			//iterate params array to bind params to Java ArrayList object
			for ($i = 0; $i < $arrayLength; $i++) {
				$param = $params[$i];
				$paramJ = new Java("com.ie.common.Param");
				$paramJ->setParamName($param[0]);
				$paramJ->setParamValue($param[1]);
				$paramJ->setParamType($param[2]);
				$lstParam->add($paramJ);
			}
		} catch (Exception $e) {
			throw $e;
		}
		
		return $lstParam;
	}
}