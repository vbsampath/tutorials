<?php

/*
 * setting css classes based on widget sizes which inturn is based on context.
 * Here widget size is small for profile context 
 */
if(isset($params['widgetSize']))
{
	if($params['widgetSize'] == WidgetSizes::Small)
	{
		$mainDivCssClass = 'reportAbusePopupBox'.$params['widgetSize'];
		$textAreaCssClass = 'reportAbuseComment'.$params['widgetSize'];
	}
}
else
{
	$mainDivCssClass = 'reportAbusePopupBox';
	$textAreaCssClass = 'reportAbuseComment';
}

/*
 * This is applied for specific bussiness cases like iCrowd-CC for now but in future it may expand to more bussiness entities.
 * Setting visibility status of report abuse widget by setting visibility of report abuse link.
 */
$buttonCssClass = 'reportAbuseLink';
if(isset($params['isVisible']))
{
	if ($params['isVisible'] == 0)
	{
		$buttonCssClass = 'reportAbuseLinkHidden';
	}
}


if ($params['authorId'] != Yii::app()->user->id) {
	if ($params['reportStatus'] == 0) {
	$ajaxUrl = Yii::app()->createUrl('site/report');
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'reportabuse-form'.$params['objectId'].$params['objectType'],
			'enableClientValidation' => true,
			'clientOptions' => array(
					'validateOnSubmit' => true,
					'afterValidate' => "js:function(form, data, hasError){
						if (!hasError) {
							jQuery.ajax({
								'type':'POST',
								'dataType':'json',
								'url':'{$ajaxUrl}',
								'cache':false,
								'data':form.serialize(),
								'beforeSend':function(){
									form.find('.reportabusebtn').val('Processing...');
									form.find('.feedbackerrorMessage').html('');
								},
								'success':function(data){
									form.find('#btnCommentSubmit_report').val('Submit');
									if(data.error=='false'){
										form.find('.feedbackerrorMessage').html('');
										form.find('.reportAbuseComment').toggleClass('ajax_loading');
										form.find('.reportAbuseLink').toggleClass('ajax_loading');
										form.find('.reportAbusePopupBox').slideUp('slow');
										form.find('.reportAbuseLink').slideUp('slow');
										form.find('.report-abuse-txt').show();
										form.find('.reportabusebtn').removeAttr('disabled',true).attr('value', 'Submit');
									}
									if(data.error == 'true'){
										form.find('.feedbackerrorMessage').html(data.message);
										setTimeout(function() {  
												form.find('.feedbackerrorMessage').fadeOut(\"slow\"); 
											},2000
										);
										form.find('.feedbackerrorMessage').show();
										form.find('.reportabusebtn').removeAttr('disabled',true).attr('value', 'Submit');
									}
								}
							});
						return false;
						}
					}",
				),
			));
	?>
	<div class="report">
	<div class="reportAbuseLink <?php echo $buttonCssClass.' reportabuse'.$params['objectType']; ?>" id="reportAbuseLink<?php echo $params['objectId'].$params['objectType']; ?>" data-type="<?php echo $params['objectType']; ?>" data-id="<?php echo $params['objectId']; ?>">
	<a href="#" class="<?php echo $params['tipsyClass']; ?>"
	title="If this <?php echo $params['objectType']; ?> does not meet the site guidelines, report it by clicking here.">
	Report Abuse
	</a>
	</div>
	<div class="clear"></div>
	<div class="<?php echo $mainDivCssClass; ?> reportAbusePopupBox" id="reportAbusePopupBox<?php echo $mainDivCssClass; ?>">
		<?php 
			echo $form->hiddenField($reportAbuseForm, 'objectId');
			echo $form->hiddenField($reportAbuseForm, 'objectType');
			echo $form->hiddenField($reportAbuseForm, 'parentId');
			echo $form->hiddenField($reportAbuseForm, 'context');
			echo $form->textArea($reportAbuseForm, 'comment',array('class' => 'toggle_text none_resize '.$textAreaCssClass, 'placeholder' => 'Enter your comment here'));
			echo $form->error($reportAbuseForm, 'comment', array('class' => 'feedbackerrorMessage')); 
		?>
		<div class="clear"></div>
		<div style="text-align: left; display: inline-block;">
		<?php echo CHtml::submitButton('Submit', array('class' => 'reportabusebtn blue-button-small', 'id'=>'btnCommentSubmit_report')); ?>
		<a href="#" class="reportAbuseCancel" id="reportAbuseCancel<?php echo $params['objectType'].$params['objectId']; ?>" style="line-height: 20px; float: left; padding-left: 5px;">Cancel</a>
		</div>
		<div style="text-align: left; display: inline-block;">
	</div>
	<div class="clear"></div>
	</div>
	<div class="report-abuse-txt" id="report-abuse-comment"
		style="display: none;">
		<?php echo Yii::t('yii', 'reportAbuse.message'); ?>
	</div>
	<?php $this->endWidget(); ?>
	</div>
	<?php
	$script = "
	//for report abuse link
	$('.reportAbuseLink').live('click',function(){
		$('.reportAbusePopupBox:visible').slideUp();
		if($(this).closest('.report').find('.reportAbusePopupBox').is(':hidden'))
		{
			$(this).closest('.report').find('.reportAbusePopupBox').slideDown();
		}
		else
		{
			$(this).closest('.report').find('.reportAbusePopupBox').slideUp();
			$(this).closest('form').resetForm();
			$(this).closest('.report').find('.feedbackerrorMessage').text('');
		}
		return false;
	});
	
	//for cancel button
	$('.reportAbuseCancel').live('click',function(){
		$(this).closest('.report').find('.reportAbusePopupBox').slideUp();
		$(this).closest('form').resetForm();
		$(this).closest('.report').find('.feedbackerrorMessage').text('');
		return false;
	});
	
	$(document).bind('click', function(e){
		var clicked = $(e.target);
		if (!(clicked.is('.reportAbusePopupBox:visible') || clicked.parents().is('.reportAbusePopupBox:visible'))) {
			$('.reportAbusePopupBox:visible').slideUp();
			$('.reportAbusePopupBox:visible').closest('form').resetForm();
		}
	})
	
	";
	Yii::app()->clientScript->registerScript('report_abuse', $script, CClientScript::POS_READY);
	 ?>
<?php } else { ?>
	<div class="report-abuse-txt">
		<?php echo Yii::t('yii', 'reportAbuse.message'); ?>
	</div>
	<?php } ?>
<?php } ?>
 