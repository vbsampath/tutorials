<?php 

/**
 * This is base action for all report abuse actions throughout the site.
 * This action handles the model validations, ajax validations, saving report abuse and stuff like that.
 * 
 * This action has come into existance because of the cost of maintainability of multiple similar actions spread over through multiple controllers.
 * Here the report abuse logic is consolidated and we can use it everywhere to report a object through our widget.
 * 
 * @package application\components\actions
 * @author Sampath Vangari <sampath.vangari@WT.com>
 */
class Report extends CAction {

    public function run() {
	    $reportAbuseForm = new ReportAbuseForm;
	    $controller = $this->getController();
	    $siteModel = new SiteModel;
	    if (isset($_POST['ReportAbuseForm'])) {
	        $reportAbuseForm->attributes = $_POST['ReportAbuseForm'];
		    try 
		    {
	    		if ($reportAbuseForm->validate()) {
	    			$isReported = $siteModel->reportAbuse($reportAbuseForm, null);
	    			echo CJSON::encode(array(
	    					'error' => 'false'
	    			));
	    			Yii::app()->end();
	    		}else{
	    			echo CJSON::encode(array(
	    					'error' => 'true',
	    					'message'=>$reportAbuseForm->getError('comment')
	    			));
	    			Yii::app()->end();
	    		}
	    	} catch (Exception $ex) {
	    		throw $ex;
	    	}
	    }
    }
}

?>